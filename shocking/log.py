from logging import Formatter, StreamHandler, getLogger, DEBUG

from click import style


COLOR_LEVELNAMES = {
    'CRITICAL': style("CRITICAL", fg="yellow"),
    'DEBUG': style("DEBUG", fg="blue"),
    'ERROR': style("ERROR", fg="red"),
    'INFO': style("INFO", fg="white"),
    'WARNING': style("WARNING", fg="yellow"),
}


class ColoredFormatter(Formatter):
    def format(self, record):
        levelname = record.levelname

        if levelname in COLOR_LEVELNAMES:
            record.levelname = COLOR_LEVELNAMES[levelname]

        return Formatter.format(self, record)


def get_formatted_logger(log_level):
    log_formats = {
        "short": "%(message)s",
        "long": "%(asctime)s | %(name)s | %(levelname)s | %(message)s"
    }

    logger = getLogger("shocking")

    logger.setLevel(log_level)

    log_format = log_formats["short"]

    if log_level == DEBUG:
        log_format = log_formats["long"]

    # if log_file is not None:
    #     log_path = Path(log_file)

    #     if not log_path.parent.is_dir():
    #         LOGGER.error("Log file directory does not exist")
    #         sys.exit(1)

    #     fh = logging.FileHandler(log_path, mode='w')
    #     fh.setLevel(logging.DEBUG)
    #     fh.setFormatter(logging.Formatter(log_format))

    #     LOGGER.addHandler(fh)

    sh = StreamHandler()

    sh.setLevel(DEBUG)

    sh.setFormatter(ColoredFormatter(log_format))

    logger.addHandler(sh)

    return logger
