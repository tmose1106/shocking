from logging import getLogger, DEBUG, ERROR, INFO, WARNING

from click import argument, group, option, help_option, version_option

from shocking import __version__


@group(name="shocking")
@option(
    "-v", "--verbose", "verbose_output",
    is_flag=True,
    help="Enable more verbose output"
)
@option(
    "-d", "--debug", "debug_output",
    is_flag=True,
    help="Enable debug output"
)
@option(
    "-q", "--quiet", "quiet_output",
    is_flag=True,
    help="Disable warning output"
)
@help_option("-h", "--help")
@version_option(__version__, "-V", "--version")
def cli(verbose_output, debug_output, quiet_output):
    """ A Shockingly Simple Site Generator """
    from shocking.log import get_formatted_logger

    log_level = WARNING

    if quiet_output:
        log_level = ERROR
    elif debug_output:
        log_level = DEBUG
    elif verbose_output:
        log_level = INFO

    logger = get_formatted_logger(log_level)

    logger.debug("Logger 'shocking' configured")


@cli.command(name="build")
@argument(
    "path",
    required=False,
    default="."
)
@option(
    "-c", "--config", "config_path",
    help="Set path to configuration file"
)
# @option(
#     "-l", "--log", "log_file",
#     help="Write output to log file"
# )
@help_option("-h", "--help")
def cli_build(path, config_path):
    """
    Build project for deployment
    """
    from shocking.core import CoreManager

    logger = getLogger("shocking")

    manager = CoreManager(path)

    logger.info("Starting build")

    for file_path, processor in manager.processors.items():
        logger.info("Processing file '%s'", file_path)
        output_path = processor(file_path)
        logger.info("Wrote contents to '%s'", output_path)

    logger.info("Build completed")


# @cli.group(name="show")
# @help_option(
#     "-h", "--help",
#     help="Print this help message",
# )
# def cli_show():
#     pass


# @cli_show.command(name="config")
# @option(
#     "-c", "--config", "config_name",
#     default="pyproject.toml",
#     help="Set path to configuration file"
# )
# @help_option(
#     "-h", "--help",
#     help="Print this help message"
# )
# def cli_show_config(config_name):
#     """ Print configuration option names and current values """
#     from json import dumps

#     from shocking import config

#     try:
#         print(dumps(config.read_from_file(config_name), indent=2))
#     except FileNotFoundError:
#         print("No '{config_name}' file found!")
#         sys.exit(1)


# @cli_show.command(name="plugins")
# @help_option(
#     "-h", "--help",
#     help="Print this help message"
# )
# def cli_show_plugins():
#     from shocking import plugin

#     for plugin_name, plugin_object in plugin.iterate("shocking.plugin"):
#         plugin_docstring = plugin_object.__doc__
#         description = plugin_docstring if plugin_docstring is not None else ''
#         print(plugin_name, description)


@cli.command(name="serve")
@argument(
    "path",
    required=False,
    default="."
)
@option(
    "-c", "--config", "config_path",
    help="Set path to configuration file"
)
@option(
    "-q", "--quiet", "quiet_output",
    is_flag=True,
    help="Hide printed output"
)
@option(
    "--debug", "debug_output",
    is_flag=True,
    help="Print debug output (implies --verbose)"
)
@option(
    "-v", "--verbose", "verbose_output",
    is_flag=True,
    help="Print more verbose output"
)
@help_option("-h", "--help")
def cli_serve(path, config_path, quiet_output, debug_output, verbose_output):
    """ Serve livereloaded project """
    from functools import partial

    from livereload import Server

    from shocking.core import CoreManager

    manager = CoreManager(path)

    logger = getLogger("shocking")

    # Build the initial project (like in the build command)
    for file_path, processor in manager.processors.items():
        processor.process(file_path)

    server = Server()

    # Register a watcher for each file, with a callback to process
    for file_path, processor in manager.processors.items():
        server.watch(str(file_path), partial(processor.process, file_path))
        logger.info("Watching file '%s'", file_path)

    server.serve(root=manager.build_dir, port=8080)
