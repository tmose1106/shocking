from collections import namedtuple
from pathlib import Path
from shutil import copy

from tomlkit import loads


File = namedtuple("File", ("content", "metadata", "path"))


class BaseProcessor:
    def __init__(self, source_path, build_path):
        self.source_path = source_path
        self.build_path = build_path

    def __call__(self, *args):
        return self.process(args[0])

    @staticmethod
    def get_destination_path(path, src, dest):
        return dest.joinpath(path.relative_to(src))

    @staticmethod
    def _parse_frontmatter(file_string, delimiter="---\n"):
        delimiter_width = len(delimiter)

        try:
            frontmatter_start_index = file_string.index(delimiter)
        except ValueError:
            return None, file_string

        try:
            frontmatter_end_index = file_string.index(
                delimiter,
                frontmatter_start_index + 1
            )
        except ValueError:
            return None, None

        return (
            # An ordered dictionary containing data from the frontmatter
            dict(loads(file_string[frontmatter_start_index + delimiter_width:frontmatter_end_index])),
            # The remained of the file's content
            file_string[frontmatter_end_index + delimiter_width:],
        )

    def load_file(self, file_path):
        return self._parse_frontmatter(file_path.read_text())


class CopyProcessor(BaseProcessor):
    def process(self, file_path):
        output_path = self.get_destination_path(file_path, self.source_path, self.build_path)

        if not output_path.parent.is_dir():
            output_path.parent.mkdir(parents=True)

        copy(file_path, output_path)

        return output_path


class GroupProcessor(BaseProcessor):
    def __init__(self, source_path, build_path, plugin_dict, group_data):
        super().__init__(source_path, build_path)

        self._get_group_plugins(plugin_dict, group_data)
        self._get_file_paths(source_path, group_data)

    def _get_group_plugins(self, plugin_dict, group_data):
        try:
            plugin_order = group_data["order"]
        except KeyError:
            raise RuntimeError("No plugin order provided")

        try:
            self.plugins = [plugin_dict[name] for name in plugin_order]
        except KeyError:
            raise RuntimeError("Could not find plugin")

    def _get_file_paths(self, source_path, group_data):
        def resolve_paths(path_string):
            if '*' in path_string:
                yield from source_path.glob(path_string)
            else:
                yield Path(source_path, path_string)

        def resolve_path_list(path_list):
            return {
                file_path
                for relative_path in path_list
                for file_path in resolve_paths(relative_path)
            }

        try:
            self.files = resolve_path_list(group_data["include"])
        except KeyError:
            raise RuntimeError("No files listed to process")

        try:
            self.files -= resolve_path_list(group_data["exclude"])
        except KeyError:
            pass  # Just continue with the includes only

    def process(self, file_path):
        frontmatter, content = self.load_file(file_path)

        file_obj = File(content, frontmatter, file_path)

        for plugin in self.plugins:
            file_obj = plugin.process(file_obj, **file_obj.metadata)

        output_path = self.get_destination_path(file_path, self.source_path, self.build_path)

        output_path.write_text(file_obj.content)

        return output_path
