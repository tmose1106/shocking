********
shocking
********

.. attention::

	The shocking API is still being developed. I would recommend not writing
	custom plugins until it is solidified.

``shocking`` is a simple, *pluggable* static **site** generator. Unlike much
of the competition, ``shocking`` is designed to ease the development of
custom, from-scratch websites; Not to only generate blogs.

Goals
=====

* Generate fully-fledged websites for any purpose
* Be completely plugin based with a small core
* Make use of the extensive Python web development library catalog
* Provided plugins with sane behavior out of the box

Dependencies
============

* Python >= 3.6
* `click`_ (handling the command line interface)
* `tomlkit`_ (parsing the configuration file and frontmatter)

.. _`click`: http://click.pocoo.org/6/
.. _`tomlkit`: https://github.com/sdispater/tomlkit

Licensing
=========

The shocking package is licensed under the terms of the `MIT/Expat license`_.

.. _`MIT/expat license`: https://spdx.org/licenses/MIT.html
