from shocking import __version__, __version_info__


def test_version():
    assert __version__ == "0.1.0"


def test_version_info():
    assert __version_info__ == (0, 1, 0)
